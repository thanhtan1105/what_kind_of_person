package com.whatkindofperson.entity;

import com.whatkindofperson.constant.EPlatform;

import javax.persistence.*;
import java.util.List;

/**
 * Created by lethanhtan on 4/30/17.
 */
@Entity
public class NotifyTokenEntity {

    @Basic
    @Column(name = "uuid", nullable = false)
    private String uuid;

    @Basic
    @Column(name = "platform", nullable = false)
    private EPlatform platform;

    @ManyToOne
    @JoinColumn(name = "account_notify")
    private AccountEntity accountEntity;

    public NotifyTokenEntity(String uuid, EPlatform platform, AccountEntity accountEntity) {
        this.uuid = uuid;
        this.platform = platform;
        this.accountEntity = accountEntity;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public EPlatform getPlatform() {
        return platform;
    }

    public void setPlatform(EPlatform platform) {
        this.platform = platform;
    }

    public AccountEntity getAccountEntity() {
        return accountEntity;
    }

    public void setAccountEntity(AccountEntity accountEntity) {
        this.accountEntity = accountEntity;
    }
}
