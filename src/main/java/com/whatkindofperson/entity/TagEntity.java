package com.whatkindofperson.entity;

import javax.persistence.*;

/**
 * Created by lethanhtan on 4/30/17.
 */
@Entity
public class TagEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Basic
    @Column(name = "name")
    private String tagName;

    @Basic
    @Column(name = "parent")
    private String parentTag;

    @Basic
    @Column(name = "isDeleted")
    private Boolean isDeleted;

    public TagEntity(String tagName, String parentTag, Boolean isDeleted) {
        this.tagName = tagName;
        this.parentTag = parentTag;
        this.isDeleted = isDeleted;
    }

    public TagEntity() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public String getParentTag() {
        return parentTag;
    }

    public void setParentTag(String parentTag) {
        this.parentTag = parentTag;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }
}
