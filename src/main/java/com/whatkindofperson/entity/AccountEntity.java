package com.whatkindofperson.entity;

import com.whatkindofperson.constant.EGender;
import com.whatkindofperson.constant.ELocation;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Set;

/**
 * Created by lethanhtan on 4/30/17.
 */
@Entity
@Table(name = "account")
public class AccountEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Basic
    @Column(name = "username", unique = true, nullable = false, length = 255)
    private String username;

    @Basic
    @Column(name = "password")
    private String password;

    @Basic
    @Column(name = "email", unique = true)
    private String email;

    @Basic
    @Column(name = "avatar")
    private String avatar;

    @Basic
    @Column(name = "location")
    private ELocation location;

    @Basic
    @Column(name = "created_at")
    private Timestamp created_at;

    @Basic
    @Column(name = "gender")
    private EGender gender;

    @Basic
    @Column(name = "accessToken")
    private String accessToken;

    @OneToMany(mappedBy = "account", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<NotifyTokenEntity> notifys;

    public AccountEntity(String username, String password, String email, String avatar, ELocation location, Timestamp created_at, EGender gender, String accessToken) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.avatar = avatar;
        this.location = location;
        this.created_at = created_at;
        this.gender = gender;
        this.accessToken = accessToken;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public ELocation getLocation() {
        return location;
    }

    public void setLocation(ELocation location) {
        this.location = location;
    }

    public Timestamp getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Timestamp created_at) {
        this.created_at = created_at;
    }

    public EGender getGender() {
        return gender;
    }

    public void setGender(EGender gender) {
        this.gender = gender;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public Set<NotifyTokenEntity> getNotifys() {
        return notifys;
    }

    public void setNotifys(Set<NotifyTokenEntity> notifys) {
        this.notifys = notifys;
    }
}
