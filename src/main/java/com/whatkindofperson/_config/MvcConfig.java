package com.whatkindofperson._config;

import org.springframework.boot.autoconfigure.web.WebMvcAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * Created by lethanhtan on 4/30/17.
 */
@Configuration
@EnableWebMvc
@ComponentScan("com.whatkindofperson._config")
public class MvcConfig extends WebMvcAutoConfiguration {


}
