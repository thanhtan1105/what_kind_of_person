package com.whatkindofperson.repository;

import com.whatkindofperson.entity.TagEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by lethanhtan on 5/1/17.
 */
@Repository
public interface TagRepo extends JpaRepository<TagEntity, Long> {


}
