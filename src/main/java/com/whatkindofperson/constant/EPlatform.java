package com.whatkindofperson.constant;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Created by lethanhtan on 4/30/17.
 */
public enum EPlatform {
    iOS (0, "iOS"),
    Android (1, "Android");

    private int index;
    private String name;

    EPlatform(int index, String name) {
        this.index = index;
        this.name = name;
    }

    public static EPlatform fromIndex(int index) {
        for (EPlatform ex: values()) {
            if (ex.getIndex() == index) {
                return ex;
            }
        }

        return null;
    }

    @JsonValue
    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
