package com.whatkindofperson.constant;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Created by lethanhtan on 4/30/17.
 */
public enum ELocation {
    ASIAN(0, "ASIAN"),
    US(1, "US");

    private int index;
    private String name;

    ELocation(int index, String name) {
        this.index = index;
        this.name = name;
    }

    public static ELocation fromIndex(int index) {
        for (ELocation ex: values()) {
            if (ex.getIndex() == index) {
                return ex;
            }
        }

        return null;
    }

    @JsonValue
    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
