package com.whatkindofperson.dto;

import com.whatkindofperson.constant.EPlatform;
import com.whatkindofperson.entity.TagEntity;

/**
 * Created by lethanhtan on 5/1/17.
 */
public class TagDTO {

    private Long id;
    private String tagName;
    private String parentTag;
    private Boolean isDeleted;

    public TagDTO() {}

    public TagDTO(TagEntity entity) {
        if (entity != null) {
            this.id = entity.getId();
            this.tagName = entity.getTagName();
            this.parentTag = entity.getParentTag();
            this.isDeleted = entity.getDeleted();
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public String getParentTag() {
        return parentTag;
    }

    public void setParentTag(String parentTag) {
        this.parentTag = parentTag;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }
}
